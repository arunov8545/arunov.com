# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


## Get a copy of the code

```sh
# Clone the repo to your system.
git clone https://github.com/ovarun/coolorette-website.git
  
# Install and Run (recommended).
npm install && npm start
```
<br>
  
## Changelog
 
<small>
	**KEY**: `📦 NEW`, `👌 IMPROVE`, `🐛 FIX`, `📖 DOC`, `🚀 RELEASE`, and `✅ TEST`
</small>

<br>


## License & Conduct

- MIT © [Arun OV](https://twitter.com/arunovelayudhan/)
- [Code of Conduct](CodeofConduct.md)


## Connect

<div align="left">
    <p>
    	<a href="https://github.com/ovarun" target="_blank">
    		<img alt="GitHub @ovarun" align="center" src="https://img.shields.io/badge/GITHUB-gray.svg?colorB=6cc644&style=flat" />
    	</a>&nbsp;
    	<small>
    		<strong>(follow)</strong> To stay up to date on free & open-source software
    	</small>
    </p>
    <p>
    	<a href="https://twitter.com/arunovelayudhan/" target="_blank">
    		<img alt="Twitter @MrAhmadAwais" align="center" src="https://img.shields.io/badge/TWITTER-gray.svg?colorB=1da1f2&style=flat" />
    	</a>&nbsp;
    	<small><strong>(follow)</strong> To get design& dev tips and tricks</small>
    </p>
    <!--p>
    	<a href="https://www.youtube.com/arunov" target="_blank">
    		<img alt="YouTube OV" align="center" src="https://img.shields.io/badge/YOUTUBE-gray.svg?colorB=ff0000&style=flat" />
    	</a>&nbsp;
    	<small><strong>(subscribe)</strong> To get design& dev tips and tricks</small>
    </p-->
    <p>
    	<a href="https://arunov.com/" target="_blank">
    		<img alt="Blog: arunov.com" align="center" src="https://img.shields.io/badge/MY%20BLOG-gray.svg?colorB=4D2AFF&style=flat" />
    	</a>&nbsp;
    	<small><strong>(read)</strong> In-depth & long form technical articles</small>
    </p>
    <p>
    	<a href="https://www.linkedin.com/in/arunovelayudhan/" target="_blank">
    		<img alt="LinkedIn @arunovelayudhan" align="center" src="https://img.shields.io/badge/LINKEDIN-gray.svg?colorB=0077b5&style=flat" />
    	</a>&nbsp;
    	<small><strong>(connect)</strong> On the LinkedIn profile y'all</small>
    </p>
</div>

<br>