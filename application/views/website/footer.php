
		<!-- start footer Area -->
		<footer class="footer-area section-gap">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 col-md-6 col-sm-6">
						<div class="single-footer-widget">
							<h4>About Me</h4>
							<p> We have tested a number of registry fix and clean utilities and present our top 3 list on our site for your convenience. </p>
							<p class="footer-text"><?php echo date('Y'); ?> Developed by <a href="<?php echo base_url(); ?>" target="_blank">Arun O.V.</a></p>
						</div>
					</div>
					<div class="col-lg-5 col-md-6 col-sm-6">
						<div class="single-footer-widget">
							<h4>Blog Subscription</h4>
							<p>Stay updated with my blog</p>
							<div class="" id="mc_embed_signup">
								<form target="_blank" action="#" method="get">
									<div class="input-group">
										<input type="text" class="form-control" name="EMAIL" placeholder="Enter Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email Address '" required="" type="email">
										<div class="input-group-btn">
											<button class="btn btn-default" type="submit">
												<span class="lnr lnr-arrow-right"></span>
											</button>    
										</div>
										<div class="info"></div>  
									</div>
								</form> 
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-6 col-sm-6 social-widget">
						<div class="single-footer-widget">
							<h4>Follow Me</h4>
							<p>Let us be social</p>
							<div class="footer-social d-flex align-items-center">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-instagram"></i></a>
								<a href="#"><i class="fa fa-linkedin"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- End footer Area -->		

		<script src="<?php echo base_url(); ?>assets/website/js/vendor/jquery-2.2.4.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/website/js/popper.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/website/js/vendor/bootstrap.min.js"></script>			
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>			
		<script src="<?php echo base_url(); ?>assets/website/js/easing.min.js"></script>			
		<script src="<?php echo base_url(); ?>assets/website/js/hoverIntent.js"></script>
		<script src="<?php echo base_url(); ?>assets/website/js/superfish.min.js"></script>	
		<script src="<?php echo base_url(); ?>assets/website/js/jquery.ajaxchimp.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/website/js/jquery.magnific-popup.min.js"></script>	
		<script src="<?php echo base_url(); ?>assets/website/js/jquery.tabs.min.js"></script>						
		<script src="<?php echo base_url(); ?>assets/website/js/jquery.nice-select.min.js"></script>	
		<script src="<?php echo base_url(); ?>assets/website/js/isotope.pkgd.min.js"></script>			
		<script src="<?php echo base_url(); ?>assets/website/js/waypoints.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/website/js/jquery.counterup.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/website/js/simple-skillbar.js"></script>							
		<script src="<?php echo base_url(); ?>assets/website/js/owl.carousel.min.js"></script>							
		<script src="<?php echo base_url(); ?>assets/website/js/mail-script.js"></script>	
		<script src="<?php echo base_url(); ?>assets/website/js/main.js"></script>	
	</body>
</html>
