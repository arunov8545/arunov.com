	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/website/img/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/website/img/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/website/img/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/website/img/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/website/img/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/website/img/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/website/img/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/website/img/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/website/img/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/website/img/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/website/img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/website/img/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/website/img/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo base_url(); ?>assets/website/img/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#8490ff">
		<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/website/img/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#8490ff">
		<!-- Author Meta -->
		<meta name="author" content="Arun O. V.">
		<!-- Meta Description -->
		<meta name="description" content="A Web Designer and Developer">
		<!-- Meta Keyword -->
		<meta name="keywords" content="web developer, web designer, developer, designer, logo designer, graphics designer,arun,arunov,ov">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Arun O. V. | Professional Designer & Web Software Engineer</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/linearicons.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/font-awesome.min.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/bootstrap.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/magnific-popup.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/jquery-ui.css">				
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/nice-select.css">							
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/animate.min.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/owl.carousel.css">				
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/main.css">			
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/main_Dark.css">
		</head>
		<body class="dark">	
			<header id="header">
				<div class="container main-menu">
					<div class="row align-items-center justify-content-between d-flex">
						<div id="logo">
							<a href="<?php echo base_url(); ?>">
								<img src="<?php echo base_url(); ?>assets/website/img/LOGO_TFW.png" alt="AOV" title="" style="width: 50px !important; height: 50px !important;"/>
							</a>
						</div>
						<nav id="nav-menu-container">
							<ul class="nav-menu">
								<li><a href="<?php echo base_url(); ?>">Home</a></li>
								<li><a href="<?php echo base_url(); ?>Web/about">About</a></li>
								<li><a href="<?php echo base_url(); ?>Web/services">Services</a></li>
								<li><a href="<?php echo base_url(); ?>Web/portfolio">Portfolio</a></li>
								<li><a href="<?php echo base_url(); ?>Web/blog">Blog</a></li>
								<li><a href="<?php echo base_url(); ?>Web/contact">Contact</a></li>
							</ul>
						</nav><!-- #nav-menu-container -->		    		
					</div>
				</div>
			</header><!-- #header -->
