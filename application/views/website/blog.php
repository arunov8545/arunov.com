
<!-- start banner Area -->
<section class="relative about-banner">	
	<div class="overlay overlay-bg"></div>
	<div class="container">				
		<div class="row d-flex align-items-center justify-content-center">
			<div class="about-content col-lg-12">
				<h1 class="text-white">
				O.V.'s Blog				
				</h1>	
				<p class="text-white link-nav"><a href="<?php echo base_url(); ?>">Home </a>  <span class="lnr lnr-arrow-right"></span><a href="<?php echo base_url(); ?>Web/blog">Blog </a></p>
			</div>	
		</div>
	</div>
</section>
<!-- End banner Area -->
			
<!-- Start top-category-widget Area -->
<section class="top-category-widget-area pt-90 pb-90 ">
	<div class="container">
		<div class="row">		

			<div class="col-lg-4">
				<div class="single-cat-widget">
					<div class="content relative">
						<div class="overlay overlay-bg"></div>
						<a href="#" target="_blank">
							<div class="thumb">
								<img class="content-image img-fluid d-block mx-auto" src="<?php echo base_url(); ?>assets/website/img/blog/cat-widget1.jpg" alt="">
							</div>
							<div class="content-details">
								<h4 class="content-title mx-auto text-uppercase">Social life</h4>
								<span></span>
								<p>Enjoy your social life together</p>
							</div>
						</a>
					</div>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="single-cat-widget">
					<div class="content relative">
						<div class="overlay overlay-bg"></div>
						<a href="#" target="_blank">
							<div class="thumb">
								<img class="content-image img-fluid d-block mx-auto" src="<?php echo base_url(); ?>assets/website/img/blog/cat-widget2.jpg" alt="">
							</div>
							<div class="content-details">
								<h4 class="content-title mx-auto text-uppercase">Politics</h4>
								<span></span>
								<p>Be a part of politics</p>
							</div>
						</a>
					</div>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="single-cat-widget">
					<div class="content relative">
						<div class="overlay overlay-bg"></div>
						<a href="#" target="_blank">
							<div class="thumb">
								<img class="content-image img-fluid d-block mx-auto" src="<?php echo base_url(); ?>assets/website/img/blog/cat-widget3.jpg" alt="">
							</div>
							<div class="content-details">
								<h4 class="content-title mx-auto text-uppercase">Food</h4>
								<span></span>
								<p>Let the food be finished</p>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>
<!-- End top-category-widget Area -->
			
<!-- Start post-content Area -->
<section class="post-content-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 posts-list">
				<div class="single-post row">
					<div class="col-lg-3  col-md-3 meta-details">
						<ul class="tags">
							<li><a href="#">Food,</a></li>
							<li><a href="#">Technology,</a></li>
							<li><a href="#">Politics,</a></li>
							<li><a href="#">Lifestyle</a></li>
						</ul>
						<div class="user-details row">
							<p class="user-name col-lg-12 col-md-12 col-6"><a href="#">Mark wiens</a> <span class="lnr lnr-user"></span></p>
							<p class="date col-lg-12 col-md-12 col-6"><a href="#">12 Dec, 2017</a> <span class="lnr lnr-calendar-full"></span></p>
							<p class="view col-lg-12 col-md-12 col-6"><a href="#">1.2M Views</a> <span class="lnr lnr-eye"></span></p>
							<p class="comments col-lg-12 col-md-12 col-6"><a href="#">06 Comments</a> <span class="lnr lnr-bubble"></span></p>
						</div>
					</div>
					<div class="col-lg-9 col-md-9 ">
						<div class="feature-img">
							<img class="img-fluid" src="<?php echo base_url(); ?>assets/website/img/blog/feature-img1.jpg" alt="">
						</div>
						<a class="posts-title" href="<?php echo base_url(); ?>Web/blog_page"><h3>Astronomy Binoculars A Great Alternative</h3></a>
						<p class="excert">
						MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction.
						</p>
						<a href="<?php echo base_url(); ?>Web/blog_page" class="primary-btn">View More</a>
					</div>
				</div>

				<nav class="blog-pagination justify-content-center d-flex">
					<ul class="pagination">
						<li class="page-item">
							<a href="#" class="page-link" aria-label="Previous">
								<span aria-hidden="true">
									<span class="lnr lnr-chevron-left"></span>
								</span>
							</a>
						</li>
						<li class="page-item"><a href="#" class="page-link">01</a></li>
						<li class="page-item active"><a href="#" class="page-link">02</a></li>
						<li class="page-item"><a href="#" class="page-link">03</a></li>
						<li class="page-item"><a href="#" class="page-link">04</a></li>
						<li class="page-item"><a href="#" class="page-link">09</a></li>
						<li class="page-item">
							<a href="#" class="page-link" aria-label="Next">
								<span aria-hidden="true">
									<span class="lnr lnr-chevron-right"></span>
								</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
			<div class="col-lg-4 sidebar-widgets">
				<div class="widget-wrap">
					<div class="single-sidebar-widget search-widget">
						<form class="search-form" action="#">
							<input placeholder="Search Posts" name="search" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Posts'" >
							<button type="submit"><i class="fa fa-search"></i></button>
						</form>
					</div>
					
					<div class="single-sidebar-widget user-info-widget">
						<img src="<?php echo base_url(); ?>assets/website/img/blog/blog_pic.png" alt="">
						<a href="<?php echo base_url(); ?>"><h4>Arun O.V.</h4></a>
						<p>
							Designer & Developer
						</p>
						<ul class="social-links">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						</ul>
					</div>

					<div class="single-sidebar-widget post-category-widget">
						<h4 class="category-title">Post Categories</h4>
						<ul class="cat-list">
							<li>
								<a href="#" class="d-flex justify-content-between">
									<p>Technology</p>
									<p>37</p>
								</a>
							</li>
							<li>
								<a href="#" class="d-flex justify-content-between">
									<p>Lifestyle</p>
									<p>24</p>
								</a>
							</li>
						</ul>
					</div>	
					<div class="single-sidebar-widget newsletter-widget">
						<h4 class="newsletter-title">Blog Post Subscription</h4>
						<div class="form-group d-flex flex-row">
							<div class="col-autos">
								<div class="input-group">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="fa fa-envelope" aria-hidden="true"></i>
										</div>
									</div>
									<input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Enter email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email'" >
								</div>
							</div>
							<a href="#" class="bbtns">Subscribe</a>
						</div>	
						<p class="text-bottom">
						You can unsubscribe at any time
						</p>								
					</div>								
				</div>
			</div>
		</div>
	</div>	
</section>
<!-- End post-content Area -->

