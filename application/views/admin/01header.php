<!doctype html>
<html class="no-js h-100" lang="en">
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Arun O V | Admin Control Panel</title>
		<meta name="description" content="Amaze Logics, Admin Control Panel">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
		<link rel="stylesheet" id="main-stylesheet" data-version="1.3.1" href="<?php echo base_url(); ?>assets/admin/styles/shards-dashboards.1.3.1.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/styles/extras.1.3.1.min.css">
		<script async defer src="https://buttons.github.io/buttons.js"></script>    
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.6/quill.snow.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.css" />
	</head>
	<body class="h-100">
		<div class="container-fluid">
			<div class="row">
				<!-- Main Sidebar -->
				<?php
					$this->load->view('admin/05sidenavigation');
				?>
	
				<!-- End Main Sidebar -->
				<!-- Header Navigation -->
				<?php
					$this->load->view('admin/04header_navigation');
				?>
				<!-- End Header Navigation -->
