
<div class="main-content-container container-fluid px-4">
	<!-- Page Header -->
	<div class="page-header row no-gutters py-4">
		<div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
			<span class="text-uppercase page-subtitle">Dashboard</span>
			<h3 class="page-title">Website</h3>
		</div>
		<div class="col-12 col-sm-4 d-flex align-items-center">
			<div class="btn-group btn-group-sm btn-group-toggle d-inline-flex mb-4 mb-sm-0 mx-auto" role="group" aria-label="Page actions">
			<a href="<?php echo base_url(); ?>admin/Dashboard" class="btn btn-white active"> Website </a>
			<a href="<?php echo base_url(); ?>admin/Dashboard/blog_overview" class="btn btn-white"> Blog </a>
			<a href="<?php echo base_url(); ?>admin/Dashboard/ecommerce" class="btn btn-white"> Admin CP </a>
		</div>
	</div>
	<div class="col-12 col-sm-4 d-flex align-items-center">
		<div id="analytics-overview-date-range" class="input-daterange input-group input-group-sm ml-auto">
			<input type="text" class="input-sm form-control" name="start" placeholder="Start Date" id="analytics-overview-date-range-1">
			<input type="text" class="input-sm form-control" name="end" placeholder="End Date" id="analytics-overview-date-range-2">
			<span class="input-group-append">
				<span class="input-group-text">
					<i class="material-icons">&#xE916;</i>
				</span>
			</span>
		</div>
	</div>
</div>
<!-- End Page Header -->

<!-- Small Stats Blocks -->
<div class="row">
	<div class="col-12 col-md-6 col-lg-3 mb-4">
		<div class="stats-small card card-small">
			<div class="card-body px-0 pb-0">
				<div class="d-flex px-3">
					<div class="stats-small__data">
						<span class="stats-small__label mb-1 text-uppercase">Total No. of Clients</span>
						<h6 class="stats-small__value count m-0">46</h6>
					</div>
					<div class="stats-small__data text-right align-items-center">
						<span class="stats-small__percentage stats-small__percentage--decrease">7.21%</span>
					</div>
				</div>
				<canvas height="60" class="analytics-overview-stats-small-2"></canvas>
			</div>
		</div>
	</div>

	<div class="col-12 col-md-6 col-lg-3 mb-4">
		<div class="stats-small card card-small">
			<div class="card-body px-0 pb-0">
				<div class="d-flex px-3">
					<div class="stats-small__data">
						<span class="stats-small__label mb-1 text-uppercase">Projects Ongoing</span>
						<h6 class="stats-small__value count m-0">07</h6>
					</div>
					<div class="stats-small__data text-right align-items-center">
						<span class="stats-small__percentage stats-small__percentage--increase">3.71%</span>
					</div>
				</div>
				<canvas height="60" class="analytics-overview-stats-small-3"></canvas>
			</div>
		</div>
	</div>

	<div class="col-12 col-md-6 col-lg-3 mb-4">
		<div class="stats-small card card-small">
			<div class="card-body px-0 pb-0">
				<div class="d-flex px-3">
					<div class="stats-small__data">
						<span class="stats-small__label mb-1 text-uppercase">Projects Completed</span>
						<h6 class="stats-small__value count m-0">27</h6>
					</div>
					<div class="stats-small__data text-right align-items-center">
						<span class="stats-small__percentage stats-small__percentage--increase">12.4%</span>
					</div>
				</div>
				<canvas height="60" class="analytics-overview-stats-small-1"></canvas>
			</div>
		</div>
	</div>

	<div class="col-12 col-md-6 col-lg-3 mb-4">
		<div class="stats-small card card-small">
			<div class="card-body px-0 pb-0">
				<div class="d-flex px-3">
					<div class="stats-small__data">
						<span class="stats-small__label mb-1 text-uppercase">No. of Testimonials</span>
						<h6 class="stats-small__value count m-0">21</h6>
					</div>
					<div class="stats-small__data text-right align-items-center">
						<span class="stats-small__percentage stats-small__percentage--decrease">2,71%</span>
					</div>
				</div>
				<canvas height="60" class="analytics-overview-stats-small-4"></canvas>
			</div>
		</div>
	</div>
</div>
<!-- End Small Stats Blocks -->

<div class="row">
	<div class="col-lg-5 mb-4">
		<div class="card card-small go-stats">
			<div class="card-header border-bottom">
				<h6 class="m-0">Web Page Hits</h6>
				<div class="block-handle"></div>
			</div>
			<div class="card-body py-0">
				<ul class="list-group list-group-small list-group-flush">
					<li class="list-group-item d-flex row px-0">
						<div class="col-lg-6 col-md-8 col-sm-8 col-6">
							<h6 class="go-stats__label mb-1">Newsletter Signups</h6>
							<div class="go-stats__meta">
								<span class="mr-2">
								<strong>291</strong> Completions</span>
								<span class="d-block d-sm-inline">
								<strong class="text-success">$192.00</strong> Value</span>
							</div>
						</div>

						<div class="col-lg-6 col-md-4 col-sm-4 col-6 d-flex">
							<div class="go-stats__value text-right ml-auto">
								<h6 class="go-stats__label mb-1">57.2%</h6>
								<span class="go-stats__meta">Conversion Rate</span>
							</div>
							<div class="go-stats__chart d-flex ml-auto">
								<canvas style="width: 45px; height: 45px;" class="my-auto" id="analytics-overview-goal-completion-1"></canvas>
							</div>
						</div>
					</li>
					
					<li class="list-group-item d-flex row px-0">

						<div class="col-lg-6 col-md-8 col-sm-8 col-6">
							<h6 class="go-stats__label mb-1">Account Creations</h6>
							<div class="go-stats__meta">
								<span class="mr-2">
								<strong>281</strong> Completions</span>
								<span class="d-block d-sm-inline">
								<strong class="text-success">$218.12</strong> Value</span>
							</div>
						</div>
						
						<div class="col-lg-6 col-md-4 col-sm-4 col-6 d-flex">
							<div class="go-stats__value text-right ml-auto">
								<h6 class="go-stats__label mb-1">30.2%</h6>
								<span class="go-stats__meta">Conversion Rate</span>
							</div>
							<div class="go-stats__chart d-flex ml-auto">
								<canvas style="width: 45px; height: 45px;" class="my-auto" id="analytics-overview-goal-completion-4"></canvas>
							</div>
						</div>
					</li>
				</ul>
			</div>

			<div class="card-footer border-top">
				<div class="row">
					<div class="col">
						<select class="custom-select custom-select-sm" style="max-width: 130px;">
							<option selected>Last Week</option>
							<option value="1">Today</option>
							<option value="2">Last Month</option>
							<option value="3">Last Year</option>
						</select>
					</div>
					<div class="col text-right view-report">
						<a href="#">View full report &rarr;</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
