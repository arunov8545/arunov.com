
				<!-- Main Sidebar -->
				<aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
					<div class="main-navbar">
						<nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
							<a class="navbar-brand w-100 mr-0" href="#" style="line-height: 25px;">
								<div class="d-table m-auto">
									<span class="d-none d-md-inline ml-1">Admin Control Panel</span>
								</div>
							</a>
							<a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
								<i class="material-icons">&#xE5C4;</i>
							</a>
						</nav>
					</div>
					<div class="nav-wrapper">
						
						<h6 class="main-sidebar__nav-title">Dashboards</h6>
						<ul class="nav nav--no-borders flex-column">
							<li class="nav-item">
								<a class="nav-link " href="<?php echo base_url(); ?>admin"><!-- active -->
									<i class="material-icons">&#xE917;</i>
									<span>Website</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/BlogOverview">
									<i class="material-icons">edit</i>
									<span>Personal Blog</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/AdminPanel">
									<i class="material-icons">&#xE8D1;</i>
									<span>Admin Panel</span>
								</a>
							</li>
						</ul>
						
						<h6 class="main-sidebar__nav-title">Website &amp; Blog</h6>
						<ul class="nav nav--no-borders flex-column">
							<li class="nav-item">
								<a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/Testimonials">
									<i class="material-icons">table_chart</i>
									<span>Testimonials</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/Portfolio">
									<i class="material-icons">view_module</i>
									<span>Portfolio</span>
								</a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
									<i class="material-icons">note_add</i>
									<span>Manage Blog</span>
								</a>
								<div class="dropdown-menu dropdown-menu-small">
									<a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/NewBlogPost"><i class="material-icons">note_add</i> New Blog Post</a>
									<a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/ViewBlogPosts"><i class="material-icons">vertical_split</i> View Blog Posts</a>
								</div>
							</li>
							<li class="nav-item">
								<a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/ContactMe">
									<i class="material-icons">table_chart</i>
									<span>Contact Me</span>
								</a>
							</li>
						</ul>
	
						<h6 class="main-sidebar__nav-title">Layouts</h6>
						<ul class="nav nav--no-borders flex-column">
							<li class="nav-item">
								<a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/header_navigation">
									<i class="material-icons">view_day</i>
									<span>Gallery</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/icon_sidebar_nav">
									<i class="material-icons">&#xE251;</i>
									<span>Social Links</span>
								</a>
							</li>
						</ul>
						<h6 class="main-sidebar__nav-title">MANAGE USER &amp; FILES</h6>
						<ul class="nav nav--no-borders flex-column">
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">
									<i class="material-icons">&#xE7FD;</i>
									<span>User Account</span>
								</a>
								<div class="dropdown-menu dropdown-menu-small">
									<a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/user_profile_lite">User Profile</a>
									<a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/edit_user_profile">Edit User Profile</a>
									<a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/login">Login</a>
									<a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/register">Register</a>
									<a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/forgot_password">Forgot Password</a>
									<a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/change_password">Change Password</a>
								</div>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
									<i class="material-icons">&#xE2C7;</i>
									<span>File Manager</span>
								</a>
								<div class="dropdown-menu dropdown-menu-small">
									<a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/file_manager_list">Files - List View</a>
									<a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/file_manager_cards">Files - Cards View</a>
								</div>
							</li>
							<li class="nav-item">
								<a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/transaction_history">
									<i class="material-icons">&#xE889;</i>
									<span>Online Visitors</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/calendar">
									<i class="material-icons">calendar_today</i>
									<span>Calendar</span>
								</a>
							</li>
						</ul>
						
					</div>
				</aside>
	

