				<main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
					<div class="main-navbar bg-white">
						<div class="container p-0">
							<!-- Main Navbar -->
							<nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
								<a class="navbar-brand" href="<?php echo base_url(); ?>admin" style="line-height: 25px;">
									<div class="d-table m-auto">
										<img id="main-logo" class="d-inline-block align-top mr-1 ml-3" style="max-width: 25px;" src="<?php echo base_url(); ?>assets/admin/images/shards-dashboards-logo.svg" alt="Shards Dashboard">
										<span class="d-none d-md-inline ml-1">ARUN OMANA VELAYUDHAN (Arun O V)</span>
									</div>
								</a>

								<ul class="navbar-nav border-left flex-row border-right ml-auto">
									<li class="nav-item border-right dropdown notifications">
										<a class="nav-link nav-link-icon text-center" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<div class="nav-link-icon__wrapper">
												<i class="material-icons">&#xE7F4;</i>
												<span class="badge badge-pill badge-danger">2</span>
											</div>
										</a>
								
										<div class="dropdown-menu dropdown-menu-small" aria-labelledby="dropdownMenuLink">
											<a class="dropdown-item" href="#">
												<div class="notification__icon-wrapper">
													<div class="notification__icon">
														<i class="material-icons">&#xE6E1;</i>
													</div>
												</div>
												<div class="notification__content">
													<span class="notification__category">Analytics</span>
													<p>Your website’s active users count increased by <span class="text-success text-semibold">28%</span> in the last week. Great job!</p>
												</div>
											</a>
								
											<a class="dropdown-item" href="#">
												<div class="notification__icon-wrapper">
													<div class="notification__icon">
														<i class="material-icons">&#xE8D1;</i>
													</div>
												</div>
												<div class="notification__content">
													<span class="notification__category">Sales</span>
													<p>Last week your store’s sales count decreased by <span class="text-danger text-semibold">5.52%</span>. It could have been worse!</p>
												</div>
											</a>
								
											<a class="dropdown-item notification__all text-center" href="#"> View all Notifications </a>
										</div>
									</li>
									
									<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
											<img class="user-avatar rounded-circle mr-2" src="<?php echo base_url(); ?>assets/admin/images/avatars/0.jpg" alt="User Avatar"> <span class="d-none d-md-inline-block">Arun O. V.</span>
										</a>
										<div class="dropdown-menu dropdown-menu-small">
											<a class="dropdown-item" href="user-profile.html"><i class="material-icons">&#xE7FD;</i> Profile</a>
											<a class="dropdown-item" href="edit-user-profile.html"><i class="material-icons">&#xE8B8;</i> Edit Profile</a>
											<a class="dropdown-item" href="file-manager-cards.html"><i class="material-icons">&#xE2C7;</i> Files</a>
											<a class="dropdown-item" href="transaction-history.html"><i class="material-icons">&#xE896;</i> Transactions</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item text-danger" href="#">
												<i class="material-icons text-danger">&#xE879;</i> Logout 
											</a>
										</div>
									</li>
								</ul>

								<nav class="nav">
									<a href="#" class="nav-link nav-link-icon toggle-sidebar  d-inline d-lg-none text-center " data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
										<i class="material-icons">&#xE5D2;</i>
									</a>
								</nav>
							</nav>
						</div> <!-- / .container -->
					</div> <!-- / .main-navbar -->
					
					<div class="header-navbar collapse d-lg-flex p-0 bg-white border-top">
						<div class="container">
							<div class="row">
								<div class="col">
									<ul class="nav nav-tabs border-0 flex-column flex-lg-row">

									<li class="nav-item dropdown">
										<a class="nav-link" data-toggle="dropdown"><i class="material-icons">&#xE8B9;</i> User Account</a>
										<div class="dropdown-menu dropdown-menu-small">
											<a href="file-manager-list.html" class="dropdown-item">User Profile</a>
											<a href="file-manager-cards.html" class="dropdown-item">Edit User Profile</a>
											<a href="login.html" class="dropdown-item">Login</a>
											<a href="register.html" class="dropdown-item">Register</a>
											<a href="forgot-password.html" class="dropdown-item">Forgot Password</a>
											<a href="change-password.html" class="dropdown-item">Change Password</a>
										</div>
									
									</li>
									
									<li class="nav-item dropdown">
										<a class="nav-link" data-toggle="dropdown"><i class="material-icons">&#xE2C7;</i> File Managers</a>
										<div class="dropdown-menu dropdown-menu-small">
											<a href="#" class="dropdown-item">Files - List View</a>
											<a href="file-manager-cards.html" class="dropdown-item">Files - Cards View</a>
										</div>
									</li>
									
									<li class="nav-item">
										<a href="transaction-history.html" class="nav-link"><i class="material-icons"></i> Online Visitors</a>
									</li>
									
									
									<li class="nav-item">
										<a href="<?php echo base_url(); ?>admin/Dashboard/calendar" class="nav-link active"><i class="material-icons">view_day</i> Calendar</a>
									</li>
										
									<li class="nav-item dropdown">
										<a class="nav-link" data-toggle="dropdown"><i class="material-icons">&#xE2C7;</i> Web Pages</a>
										<div class="dropdown-menu dropdown-menu-small">
											<a href="index-2.html" class="dropdown-item">Home</a>
											<a href="ecommerce.html" class="dropdown-item">About</a>
											<a href="blog-overview.html" class="dropdown-item">Services</a>
										</div>
									</li>
										
										
									<li class="nav-item">
										<a href="icon-sidebar-nav.html" class="nav-link"><i class="material-icons">&#xE251;</i> Reserve 1</a>
									</li>
								
									<li class="nav-item dropdown">
										<a class="nav-link" data-toggle="dropdown"><i class="material-icons">view_module</i> Reserve 2</a>
										<div class="dropdown-menu dropdown-menu-small">
											<a href="components-overview.html" class="dropdown-item">Overview</a>
											<a href="components-blog-posts.html" class="dropdown-item">Blog Posts</a>
										</div>
									</li>
									
									
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="main-content-container container">



				</div>
