
					<footer class="main-footer d-flex p-2 px-3 bg-white border-top">
						<ul class="nav">
							<li class="nav-item">
								<a class="nav-link" href="#">Home</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Services</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">About</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Products</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Blog</a>
							</li>
						</ul>
						<span class="copyright ml-auto my-auto mr-2">&copy; <?php
								  $fromYear = 2017; 
								  $thisYear = (int)date('Y'); 
								  echo $fromYear . (($fromYear != $thisYear) ? '-' . $thisYear : '');?>. Developed by Arun O V</span>
					</footer>
				</main>
			</div>
		</div>
		
		<script src="https://www.gstatic.com/charts/loader.js"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
		<script src="https://unpkg.com/shards-ui@latest/dist/js/shards.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/scripts/extras.1.3.1.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/scripts/shards-dashboards.1.3.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.6/quill.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/scripts/app/app-blog-new-post.1.3.1.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/scripts/app/app-analytics-overview.1.3.1.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/scripts/app/app-ecommerce.1.3.1.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/scripts/app/app-blog-overview.1.3.1.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/scripts/app/app-user-profile.1.3.1.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/scripts/app/app-edit-user-profile.1.3.1.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/min/dropzone.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/scripts/app/app-file-manager.1.3.1.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/scripts/app/app-transaction-history.1.3.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/scripts/app/app-calendar.1.3.1.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/scripts/app/app-components-overview.1.3.1.min.js"></script>
	</body>
</html>
