
        <main class="main-content col">
          <div class="main-content-container container-fluid px-4 my-auto h-100">
            <div class="row no-gutters h-100">
              <div class="col-lg-3 col-md-5 auth-form mx-auto my-auto">
                <div class="card">
                  <div class="card-body">
                    <img class="auth-form__logo d-table mx-auto mb-3" src="<?php echo base_url(); ?>assets/admin/images/shards-dashboards-logo.svg" alt="Shards Dashboards - Register Template">
                    <h5 class="auth-form__title text-center mb-4">Reset Password</h5>
                    <form>
                      <div class="form-group mb-4">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        <small id="emailHelp" class="form-text text-muted text-center">You will receive an email with a unique token.</small>
                      </div>
                      <button type="submit" class="btn btn-pill btn-accent d-table mx-auto">Reset Password</button>
                    </form>
                  </div>
                </div>
                <div class="auth-form__meta d-flex mt-4">
                  <a class="mx-auto" href="<?php echo base_url(); ?>admin/Dashloard/login">Take me back to login.</a>
                </div>
              </div>
            </div>
          </div>