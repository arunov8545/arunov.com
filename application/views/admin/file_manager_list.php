
          <div class="main-content-container container-fluid px-4 pb-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col">
                <span class="text-uppercase page-subtitle">Dashboards</span>
                <h3 class="page-title">File Manager</h3>
              </div>
              <div class="col d-flex">
                <div class="btn-group btn-group-sm d-inline-flex ml-auto my-auto" role="group" aria-label="Table row actions">
                  <a href="<?php echo base_url(); ?>admin/Dashboard/file_manager_list" class="btn btn-white active">
                    <i class="material-icons">&#xE8EF;</i>
                  </a>
                  <a href="<?php echo base_url(); ?>admin/Dashboard/file_manager_cards" class="btn btn-white">
                    <i class="material-icons">&#xE8F0;</i>
                  </a>
                </div>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- File Manager -->
            <table class="file-manager file-manager-list d-none table-responsive">
              <thead>
                <tr>
                  <th colspan="5" class="text-left bg-white">
                    <form action="<?php echo base_url(); ?>admin/Dashboard/file-upload" class="dropzone"></form>
                  </th>
                </tr>
                <tr>
                  <th style="width: 10px;" class="hide-sort-icons"></th>
                  <th class="text-left">Name</th>
                  <th>Size</th>
                  <th>Type</th>
                  <th class="text-right">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="file-manager__item-icon">
                    <div>
                      <i class="material-icons">&#xE2C7;</i>
                    </div>
                  </td>
                  <td class="text-left">
                    <h5 class="file-manager__item-title">Projects</h5>
                    <span class="file-manager__item-meta">Last opened 12 days ago.</span>
                  </td>
                  <td>7 GB</td>
                  <td>Directory</td>
                  <td class="file-manager__item-actions">
                    <div class="btn-group btn-group-sm d-flex justify-content-end" role="group" aria-label="Table row actions">
                      <button type="button" class="btn btn-white active-light">
                        <i class="material-icons">&#xE254;</i>
                      </button>
                      <button type="button" class="btn btn-danger">
                        <i class="material-icons">&#xE872;</i>
                      </button>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="file-manager__item-icon">
                    <div>
                      <i class="material-icons">&#xE2C7;</i>
                    </div>
                  </td>
                  <td class="text-left">
                    <h5 class="file-manager__item-title">Movies</h5>
                    <span class="file-manager__item-meta">Last opened 4 hours ago.</span>
                  </td>
                  <td>182 GB</td>
                  <td>Directory</td>
                  <td class="file-manager__item-actions">
                    <div class="btn-group btn-group-sm d-flex justify-content-end" role="group" aria-label="Table row actions">
                      <button type="button" class="btn btn-white active-light">
                        <i class="material-icons">&#xE254;</i>
                      </button>
                      <button type="button" class="btn btn-danger">
                        <i class="material-icons">&#xE872;</i>
                      </button>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="file-manager__item-icon">
                    <div>
                      <i class="material-icons">&#xE2C7;</i>
                    </div>
                  </td>
                  <td class="text-left">
                    <h5 class="file-manager__item-title">Backups</h5>
                    <span class="file-manager__item-meta">Last opened 2 hours ago.</span>
                  </td>
                  <td>2TB</td>
                  <td>Directory</td>
                  <td class="file-manager__item-actions">
                    <div class="btn-group btn-group-sm d-flex justify-content-end" role="group" aria-label="Table row actions">
                      <button type="button" class="btn btn-white active-light">
                        <i class="material-icons">&#xE254;</i>
                      </button>
                      <button type="button" class="btn btn-danger">
                        <i class="material-icons">&#xE872;</i>
                      </button>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="file-manager__item-icon">
                    <div>
                      <i class="material-icons">&#xE2C7;</i>
                    </div>
                  </td>
                  <td class="text-left">
                    <h5 class="file-manager__item-title">Photos</h5>
                    <span class="file-manager__item-meta">Last opened yesterday.</span>
                  </td>
                  <td>1.29 GB</td>
                  <td>Directory</td>
                  <td class="file-manager__item-actions">
                    <div class="btn-group btn-group-sm d-flex justify-content-end" role="group" aria-label="Table row actions">
                      <button type="button" class="btn btn-white active-light">
                        <i class="material-icons">&#xE254;</i>
                      </button>
                      <button type="button" class="btn btn-danger">
                        <i class="material-icons">&#xE872;</i>
                      </button>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="file-manager__item-icon">
                    <div>
                      <i class="material-icons">&#xE2C7;</i>
                    </div>
                  </td>
                  <td class="text-left">
                    <h5 class="file-manager__item-title">Old Files</h5>
                    <span class="file-manager__item-meta">Last opened 3 months ago.</span>
                  </td>
                  <td>9.63 GB</td>
                  <td>Directory</td>
                  <td class="file-manager__item-actions">
                    <div class="btn-group btn-group-sm d-flex justify-content-end" role="group" aria-label="Table row actions">
                      <button type="button" class="btn btn-white active-light">
                        <i class="material-icons">&#xE254;</i>
                      </button>
                      <button type="button" class="btn btn-danger">
                        <i class="material-icons">&#xE872;</i>
                      </button>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="file-manager__item-icon">
                    <div>
                      <i class="material-icons">&#xE2C7;</i>
                    </div>
                  </td>
                  <td class="text-left">
                    <h5 class="file-manager__item-title">New Folder</h5>
                    <span class="file-manager__item-meta">Last opened 3 minutes ago.</span>
                  </td>
                  <td>0 KB</td>
                  <td>Directory</td>
                  <td class="file-manager__item-actions">
                    <div class="btn-group btn-group-sm d-flex justify-content-end" role="group" aria-label="Table row actions">
                      <button type="button" class="btn btn-white active-light">
                        <i class="material-icons">&#xE254;</i>
                      </button>
                      <button type="button" class="btn btn-danger">
                        <i class="material-icons">&#xE872;</i>
                      </button>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="file-manager__item-icon">
                    <div>
                      <i class="material-icons">&#xE24D;</i>
                    </div>
                  </td>
                  <td class="text-left">
                    <h5 class="file-manager__item-title">01_project_description.doc</h5>
                    <span class="file-manager__item-meta">Last opened 3 days ago.</span>
                  </td>
                  <td>23 KB</td>
                  <td>Document</td>
                  <td class="file-manager__item-actions">
                    <div class="btn-group btn-group-sm d-flex justify-content-end" role="group" aria-label="Table row actions">
                      <button type="button" class="btn btn-white active-light">
                        <i class="material-icons">&#xE254;</i>
                      </button>
                      <button type="button" class="btn btn-danger">
                        <i class="material-icons">&#xE872;</i>
                      </button>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="file-manager__item-icon">
                    <div>
                      <i class="material-icons">&#xE24D;</i>
                    </div>
                  </td>
                  <td class="text-left">
                    <h5 class="file-manager__item-title">client_feedback.doc</h5>
                    <span class="file-manager__item-meta">Last opened 6 days ago.</span>
                  </td>
                  <td>86 KB</td>
                  <td>Document</td>
                  <td class="file-manager__item-actions">
                    <div class="btn-group btn-group-sm d-flex justify-content-end" role="group" aria-label="Table row actions">
                      <button type="button" class="btn btn-white active-light">
                        <i class="material-icons">&#xE254;</i>
                      </button>
                      <button type="button" class="btn btn-danger">
                        <i class="material-icons">&#xE872;</i>
                      </button>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="file-manager__item-icon">
                    <div>
                      <i class="material-icons">&#xE24D;</i>
                    </div>
                  </td>
                  <td class="text-left">
                    <h5 class="file-manager__item-title">wedding_photos_001.zip</h5>
                    <span class="file-manager__item-meta">Last opened 2 weeks ago.</span>
                  </td>
                  <td>186 MB</td>
                  <td>Document</td>
                  <td class="file-manager__item-actions">
                    <div class="btn-group btn-group-sm d-flex justify-content-end" role="group" aria-label="Table row actions">
                      <button type="button" class="btn btn-white active-light">
                        <i class="material-icons">&#xE254;</i>
                      </button>
                      <button type="button" class="btn btn-danger">
                        <i class="material-icons">&#xE872;</i>
                      </button>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
            <!-- End File Manager -->
          </div> 