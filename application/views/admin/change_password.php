
        <main class="main-content col">
          <div class="main-content-container container-fluid px-4 my-auto h-100">
            <div class="row no-gutters h-100">
              <div class="col-lg-3 col-md-5 auth-form mx-auto my-auto">
                <div class="card">
                  <div class="card-body">
                    <img class="auth-form__logo d-table mx-auto mb-3" src="<?php echo base_url(); ?>assets/admin/images/shards-dashboards-logo.svg" alt="Shards Dashboards - Register Template">
                    <h5 class="auth-form__title text-center mb-4">Change Password</h5>
                    <form>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                      </div>
                      <div class="form-group mb-4">
                        <label for="exampleInputPassword2">Repeat Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Repeat Password">
                      </div>
                      <button type="submit" class="btn btn-pill btn-accent d-table mx-auto">Change Password</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>