
<style type="text/css">
    textarea { border: none; overflow: auto; }
</style>

<div class="main-content-container container-fluid px-4">
	<!-- Page Header -->
	<div class="page-header row no-gutters py-4">
		<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
			<span class="text-uppercase page-subtitle">Incoming</span>
			<h3 class="page-title">Contact Me Requests</h3>
		</div>
	</div>
	<!-- End Page Header -->

	<!-- Default Light Table -->
	<div class="row">
		<div class="col">
			<div class="card card-small mb-4">
				<div class="card-header border-bottom">
					<h6 class="m-0">Contact Form Submissions</h6>
				</div>
				<div class="card-body p-0 pb-3 text-center">
					<table class="table mb-0">
						<thead class="bg-light">
							<tr>
								<th scope="col" class="border-0">#</th>
								<th scope="col" class="border-0">Full Name</th>
								<th scope="col" class="border-0">Email Address</th>
								<th scope="col" class="border-0">Contact No.</th>
								<th scope="col" class="border-0">Message</th>
								<th scope="col" class="border-0">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Ali Kerry</td>
								<td>ali@cusat.ac.in</td>
								<td>1234567890</td>
								<td>
									<textarea rows="2" cols="25" readonly>GdańskGdańskGdańskGdańsk GdańskGdańskGdańsk</textarea>
								</td>
								<td>
									<button type="button" class="mb-2 btn btn-sm btn-outline-primary mr-1">Primary</button>
									<button type="button" class="mb-2 btn btn-sm btn-outline-success mr-1">Success</button>
									<button type="button" class="mb-2 btn btn-sm btn-outline-danger mr-1">Danger</button>
									<button type="button" class="mb-2 btn btn-sm btn-outline-royal-blue mr-1">Royal Blue</button>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Clark</td>
								<td>Angela</td>
								<td>Estonia</td>
								<td>Borghetto di Vara</td>
								<td>1-660-850-1647</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Jerry</td>
								<td>Nathan</td>
								<td>Cyprus</td>
								<td>Braunau am Inn</td>
								<td>214-4225</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Colt</td>
								<td>Angela</td>
								<td>Liberia</td>
								<td>Bad Hersfeld</td>
								<td>1-848-473-7416</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- End Default Light Table -->
</div>
