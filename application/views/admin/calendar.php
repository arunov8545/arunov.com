
          <div class="main-content-container container-fluid px-4 mb-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">Dashboard</span>
                <h3 class="page-title">Calendar</h3>
              </div>
              <div class="col-12 col-sm-6 d-flex align-items-center">
                <div class="d-inline-flex mb-sm-0 mx-auto ml-sm-auto mr-sm-0" role="group" aria-label="Page actions">
                  <a id="add-new-event" href="<?php echo base_url(); ?>admin/" class="btn btn-primary">
                    <i class="material-icons">add</i> New Event </a>
                </div>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Calendar -->
            <div class="card">
              <div class="card-body py-4">
                <div id="calendar"></div>
              </div>
            </div>
            <!-- End Calendar -->
          </div>