
<div class="main-content-container container-fluid px-4">
	<!-- Page Header -->
	<div class="page-header row no-gutters py-4">
		<div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
			<span class="text-uppercase page-subtitle">Dashboard</span>
			<h3 class="page-title">Admin Control Panel</h3>
		</div>
		<div class="col-12 col-sm-4 d-flex align-items-center">
			<div class="btn-group btn-group-sm btn-group-toggle d-inline-flex mb-4 mb-sm-0 mx-auto" role="group" aria-label="Page actions">
				<a href="<?php echo base_url(); ?>admin/Dashboard" class="btn btn-white"> Website </a>
				<a href="<?php echo base_url(); ?>admin/Dashboard/blog_overview" class="btn btn-white"> Blog </a>
				<a href="<?php echo base_url(); ?>admin/Dashboard/ecommerce" class="btn btn-white active"> Admin CP </a>
			</div>
		</div>
		<div class="col-12 col-sm-4 d-flex align-items-center">
			<div id="sales-overview-date-range" class="input-daterange input-group input-group-sm ml-auto">
				<input type="text" class="input-sm form-control" name="start" placeholder="Start Date" id="analytics-overview-date-range-1">
				<input type="text" class="input-sm form-control" name="end" placeholder="End Date" id="analytics-overview-date-range-2">
				<span class="input-group-append">
					<span class="input-group-text">
						<i class="material-icons">&#xE916;</i>
					</span>
				</span>
			</div>
		</div>
	</div>
	<!-- End Page Header -->

	<!-- Small Stats Blocks -->
	<div class="row">
		<div class="col-lg-3 col-md-6 col-sm-12 mb-4">
			<div class="stats-small card card-small">
				<div class="card-body px-0 pb-0">
					<div class="d-flex px-3">
						<div class="stats-small__data">
							<span class="stats-small__label mb-1 text-uppercase">Total Revenue</span>
							<h6 class="stats-small__value count m-0">$29,219</h6>
						</div>
						<div class="stats-small__data text-right">
							<span class="stats-small__percentage stats-small__percentage--increase">2.93%</span>
						</div>
					</div>
					<canvas height="53" class="sales-overview-small-stats-1"></canvas>
				</div>
			</div>
		</div>
		
		<div class="col-lg-3 col-md-6 col-sm-12 mb-4">
			<div class="stats-small card card-small">
				<div class="card-body px-0 pb-0">
					<div class="d-flex px-3">
						<div class="stats-small__data">
							<span class="stats-small__label mb-1 text-uppercase">Revenue Today</span>
							<h6 class="stats-small__value count m-0">$8,391</h6>
						</div>
						<div class="stats-small__data text-right">
							<span class="stats-small__percentage stats-small__percentage--decrease">7.21%</span>
						</div>
					</div>
					<canvas height="53" class="sales-overview-small-stats-2"></canvas>
				</div>
			</div>
		</div>
	
		<div class="col-lg-3 col-md-6 col-sm-12 mb-4">
			<div class="stats-small card card-small">
				<div class="card-body px-0 pb-0">
					<div class="d-flex px-3">
						<div class="stats-small__data">
							<span class="stats-small__label mb-1 text-uppercase">Total Customers</span>
							<h6 class="stats-small__value count m-0">891</h6>
						</div>
						<div class="stats-small__data text-right">
							<span class="stats-small__percentage stats-small__percentage--increase">3,71%</span>
						</div>
					</div>
					<canvas height="53" class="sales-overview-small-stats-3"></canvas>
				</div>
			</div>
		</div>
	
		<div class="col-lg-3 col-md-6 col-sm-12 mb-4">
			<div class="stats-small card card-small">
				<div class="card-body px-0 pb-0">
					<div class="d-flex px-3">
						<div class="stats-small__data">
							<span class="stats-small__label mb-1 text-uppercase">New Customers</span>
							<h6 class="stats-small__value count m-0">29</h6>
						</div>
						<div class="stats-small__data text-right">
							<span class="stats-small__percentage stats-small__percentage--decrease">2.71%</span>
						</div>
					</div>
					<canvas height="53" class="sales-overview-small-stats-4"></canvas>
				</div>
			</div>
		</div>
	</div>
	<!-- End Small Stats Blocks -->

	<div class="row">
		<div class="col col-lg-8 col-md-12 mb-4">
			<div class="card card-small lo-stats h-100">
				<div class="card-header border-bottom">
					<h6 class="m-0">General Settings</h6>
					<div class="block-handle"></div>
				</div>
				<div class="card-body p-0">
					<div class="container-fluid px-0">
						<table class="table mb-0">
							<thead class="py-2 bg-light text-semibold border-bottom">
								<tr>
									<th>Details</th>
									<th></th>
									<th class="text-center">Status</th>
									<th class="text-center">Items</th>
									<th class="text-center">Total</th>
									<th class="text-right">Actions</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="lo-stats__image">
										<img class="border rounded" src="<?php echo base_url(); ?>assets/admin/images/sales-overview/product-order-1.jpg">
									</td>
									<td class="lo-stats__order-details">
										<span>#19280</span>
										<span>21 February 2018 20:32</span>
									</td>
									<td class="lo-stats__status">
										<div class="d-table mx-auto">
											<span class="badge badge-pill badge-success">Complete</span>
										</div>
									</td>
									<td class="lo-stats__items text-center">12</td>
									<td class="lo-stats__total text-center text-success">$199</td>
									<td class="lo-stats__actions">
										<div class="btn-group d-table ml-auto" role="group" aria-label="Basic example">
											<button type="button" class="btn btn-sm btn-white">Cancel</button>
											<button type="button" class="btn btn-sm btn-white">Edit</button>
										</div>
									</td>
								</tr>
								<tr>
									<td class="lo-stats__image">
										<img class="border rounded" src="<?php echo base_url(); ?>assets/admin/images/sales-overview/product-order-2.jpg">
									</td>
									<td class="lo-stats__order-details">
										<span>#19279</span>
										<span>21 February 2018 20:32</span>
									</td>
									<td class="lo-stats__status">
										<div class="d-table mx-auto">
											<span class="badge badge-pill badge-warning">Pending</span>
										</div>
									</td>
									<td class="lo-stats__items text-center">7</td>
									<td class="lo-stats__total text-center text-success">$612</td>
									<td class="lo-stats__actions">
										<div class="btn-group d-table ml-auto" role="group" aria-label="Basic example">
											<button type="button" class="btn btn-sm btn-white">Cancel</button>
											<button type="button" class="btn btn-sm btn-white">Edit</button>
										</div>
									</td>
								</tr>
								<tr>
									<td class="lo-stats__image">
										<img class="border rounded" src="<?php echo base_url(); ?>assets/admin/images/sales-overview/product-order-3.jpg">
									</td>
									<td class="lo-stats__order-details">
										<span>#19278</span>
										<span>21 February 2018 20:32</span>
									</td>
									<td class="lo-stats__status">
										<div class="d-table mx-auto">
											<span class="badge badge-pill badge-danger">Canceled</span>
										</div>
									</td>
									<td class="lo-stats__items text-center">18</td>
									<td class="lo-stats__total text-center text-success">$1211</td>
									<td class="lo-stats__actions">
										<div class="btn-group d-table ml-auto" role="group" aria-label="Basic example">
											<button type="button" class="btn btn-sm btn-white">Cancel</button>
											<button type="button" class="btn btn-sm btn-white">Edit</button>
										</div>
									</td>
								</tr>
								<tr>
									<td class="lo-stats__image">
										<img class="border rounded" src="<?php echo base_url(); ?>assets/admin/images/sales-overview/product-sweaters.jpg">
									</td>
									<td class="lo-stats__order-details">
										<span>#19277</span>
										<span>21 February 2018 20:32</span>
									</td>
									<td class="lo-stats__status">
										<div class="d-table mx-auto">
											<span class="badge badge-pill badge-success">Complete</span>
										</div>
									</td>
									<td class="lo-stats__items text-center">12</td>
									<td class="lo-stats__total text-center text-success">$199</td>
									<td class="lo-stats__actions">
										<div class="btn-group d-table ml-auto" role="group" aria-label="Basic example">
											<button type="button" class="btn btn-sm btn-white">Cancel</button>
											<button type="button" class="btn btn-sm btn-white">Edit</button>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="card-footer border-top">
					<div class="row">
						<div class="col">
							<select class="custom-select custom-select-sm" style="max-width: 130px;">
								<option selected>Last Week</option>
								<option value="1">Today</option>
								<option value="2">Last Month</option>
								<option value="3">Last Year</option>
							</select>
						</div>
						<div class="col text-right view-report">
							<a href="#">View full report &rarr;</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="col-lg-4 col-md-12 mb-4">
			<!-- Sales by Category -->
			<div class="card sc-stats card-small h-100">
				<div class="card-header border-bottom">
					<h6 class="m-0">Online Visitors</h6>
					<div class="block-handle"></div>
				</div>
				<div class="card-body p-0">
					<div class="container-fluid">
						<div class="row px-3">
							<div class="col-2 sc-stats__image">
								<img class="border rounded" src="<?php echo base_url(); ?>assets/admin/images/sales-overview/product-shoes.jpg">
							</div>
							<div class="col sc-stats__title">Shoes</div>
							<div class="col-2 sc-stats__value text-right">12,281</div>
							<div class="col-3 sc-stats__percentage text-right">32.4%</div>
						</div>
						
						<div class="row px-3">
							<div class="col-2 sc-stats__image">
								<img class="border rounded" src="<?php echo base_url(); ?>assets/admin/images/sales-overview/no-product-image.jpg">
							</div>
							<div class="col sc-stats__title">Men's Jeans</div>
							<div class="col-2 sc-stats__value text-right">8,129</div>
							<div class="col-3 sc-stats__percentage text-right">28.4%</div>
						</div>
				
						<div class="row px-3">
							<div class="col-2 sc-stats__image">
								<img class="border rounded" src="<?php echo base_url(); ?>assets/admin/images/sales-overview/product-sportswear.jpg">
							</div>
							<div class="col sc-stats__title">Sportswear</div>
							<div class="col-2 sc-stats__value text-right">812</div>
							<div class="col-3 sc-stats__percentage text-right">12.2%</div>
						</div>
						
						<div class="row px-3">
							<div class="col-2 sc-stats__image">
								<img class="border rounded" src="<?php echo base_url(); ?>assets/admin/images/sales-overview/product-basics.jpg">
							</div>
							<div class="col sc-stats__title">Basics</div>
							<div class="col-2 sc-stats__value text-right">29</div>
							<div class="col-3 sc-stats__percentage text-right">7.1%</div>
						</div>
						
						<div class="row px-3">
							<div class="col-2 sc-stats__image">
								<img class="border rounded" src="<?php echo base_url(); ?>assets/admin/images/sales-overview/product-sweaters.jpg">
							</div>
							<div class="col sc-stats__title">Sweaters</div>
							<div class="col-2 sc-stats__value text-right">3</div>
							<div class="col-3 sc-stats__percentage text-right">1.2%</div>
						</div>
					</div>
				</div>
				<div class="card-footer border-top">
					<div class="row">
						<div class="col">
							<select class="custom-select custom-select-sm">
								<option selected>Last Week</option>
								<option value="1">Today</option>
								<option value="2">Last Month</option>
								<option value="3">Last Year</option>
							</select>
						</div>
						<div class="col text-right view-report">
							<a href="#">Full report &rarr;</a>
						</div>
					</div>
				</div>
			</div>
			<!-- End Sales by Category -->
		</div>
	</div>
</div>
