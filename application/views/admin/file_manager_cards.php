
          <div class="main-content-container container-fluid px-4 mb-3">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col">
                <span class="text-uppercase page-subtitle">Dashboards</span>
                <h3 class="page-title">File Manager</h3>
              </div>
              <div class="col d-flex">
                <div class="btn-group btn-group-sm d-inline-flex ml-auto my-auto" role="group" aria-label="Table row actions">
                  <a href="<?php echo base_url(); ?>admin/Dashboard/file_manager_list" class="btn btn-white">
                    <i class="material-icons">&#xE8EF;</i>
                  </a>
                  <a href="<?php echo base_url(); ?>admin/Dashboard/file_manager_cards" class="btn btn-white active">
                    <i class="material-icons">&#xE8F0;</i>
                  </a>
                </div>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- File Manager - Cards -->
            <div class="file-manager file-manager-cards">
              <div class="card card-small mb-3">
                <div class="row no-gutters border-bottom">
                  <div class="file-manager-cards__dropzone w-100 p-2">
                    <form action="<?php echo base_url(); ?>admin/Dashboard/file-upload" class="dropzone"></form>
                  </div>
                </div>
                <div class="row no-gutters p-2">
                  <div class="col-lg-3 mb-2 mb-lg-0">
                    <form action="https://designrevision.com/demo/shards-dashboards/POST">
                      <div class="input-group input-group-seamless">
                        <div class="input-group-prepend">
                          <div class="input-group-text">
                            <i class="material-icons">&#xE8B6;</i>
                          </div>
                        </div>
                        <input type="text" class="form-control form-control-sm file-manager-cards__search" placeholder="Search files">
                      </div>
                    </form>
                  </div>
                  <div class="col">
                    <div class="d-flex ml-lg-auto my-auto">
                      <div class="btn-group btn-group-sm mr-2 ml-lg-auto" role="group" aria-label="Table row actions">
                        <button type="button" class="btn btn-white">
                          <i class="material-icons">&#xE5CA;</i>
                        </button>
                        <button type="button" class="btn btn-white">
                          <i class="material-icons">&#xE870;</i>
                        </button>
                        <button type="button" class="btn btn-white">
                          <i class="material-icons">&#xE254;</i>
                        </button>
                        <button type="button" class="btn btn-white">
                          <i class="material-icons">&#xE872;</i>
                        </button>
                      </div>
                      <button class="btn btn-sm btn-accent d-inline-block ml-auto ml-lg-0">
                        <i class="material-icons">&#xE145;</i> Add New </button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <span class="file-manager__group-title text-uppercase text-light">Directories</span>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-3">
                  <div class="file-manager__item file-manager__item--directory file-manager__item--selected card card-small mb-3">
                    <div class="card-footer">
                      <span class="file-manager__item-icon">
                        <i class="material-icons">&#xE2C7;</i>
                      </span>
                      <h6 class="file-manager__item-title">Projects</h6>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="file-manager__item file-manager__item--directory card card-small mb-3">
                    <div class="card-footer">
                      <span class="file-manager__item-icon">
                        <i class="material-icons">&#xE2C7;</i>
                      </span>
                      <h6 class="file-manager__item-title">Movies</h6>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="file-manager__item file-manager__item--directory card card-small mb-3">
                    <div class="card-footer">
                      <span class="file-manager__item-icon">
                        <i class="material-icons">&#xE2C7;</i>
                      </span>
                      <h6 class="file-manager__item-title">Backups</h6>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="file-manager__item file-manager__item--directory card card-small mb-3">
                    <div class="card-footer">
                      <span class="file-manager__item-icon">
                        <i class="material-icons">&#xE2C7;</i>
                      </span>
                      <h6 class="file-manager__item-title">Photos</h6>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="file-manager__item file-manager__item--directory card card-small mb-3">
                    <div class="card-footer">
                      <span class="file-manager__item-icon">
                        <i class="material-icons">&#xE2C7;</i>
                      </span>
                      <h6 class="file-manager__item-title">Old Files</h6>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="file-manager__item file-manager__item--directory card card-small mb-3">
                    <div class="card-footer">
                      <span class="file-manager__item-icon">
                        <i class="material-icons">&#xE2C7;</i>
                      </span>
                      <h6 class="file-manager__item-title">New Folder With Extremely Long Name</h6>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <span class="file-manager__group-title text-uppercase text-light">Documents</span>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-sm-6 col-lg-3">
                  <div class="file-manager__item card card-small mb-3">
                    <div class="file-manager__item-preview card-body px-0 pb-0 pt-4">
                      <img src="<?php echo base_url(); ?>assets/admin/images/file-manager/document-preview-1.jpg" alt="File Manager - Item Preview">
                    </div>
                    <div class="card-footer border-top">
                      <span class="file-manager__item-icon">
                        <i class="material-icons">&#xE24D;</i>
                      </span>
                      <h6 class="file-manager__item-title">Lorem Ipsum Document</h6>
                      <span class="file-manager__item-size ml-auto">12kb</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                  <div class="file-manager__item card card-small mb-3">
                    <div class="file-manager__item-preview card-body px-0 pb-0 pt-4">
                      <img src="<?php echo base_url(); ?>assets/admin/images/file-manager/document-preview-1.jpg" alt="File Manager - Item Preview">
                    </div>
                    <div class="card-footer border-top">
                      <span class="file-manager__item-icon">
                        <i class="material-icons">&#xE24D;</i>
                      </span>
                      <h6 class="file-manager__item-title">Lorem Ipsum Document</h6>
                      <span class="file-manager__item-size ml-auto">12kb</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                  <div class="file-manager__item card card-small mb-3">
                    <div class="file-manager__item-preview card-body px-0 pb-0 pt-4">
                      <img src="<?php echo base_url(); ?>assets/admin/images/file-manager/document-preview-1.jpg" alt="File Manager - Item Preview">
                    </div>
                    <div class="card-footer border-top">
                      <span class="file-manager__item-icon">
                        <i class="material-icons">&#xE24D;</i>
                      </span>
                      <h6 class="file-manager__item-title">Lorem Ipsum Document</h6>
                      <span class="file-manager__item-size ml-auto">12kb</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                  <div class="file-manager__item card card-small mb-3">
                    <div class="file-manager__item-preview card-body px-0 pb-0 pt-4">
                      <img src="<?php echo base_url(); ?>assets/admin/images/file-manager/document-preview-1.jpg" alt="File Manager - Item Preview">
                    </div>
                    <div class="card-footer border-top">
                      <span class="file-manager__item-icon">
                        <i class="material-icons">&#xE24D;</i>
                      </span>
                      <h6 class="file-manager__item-title">Lorem Ipsum Document</h6>
                      <span class="file-manager__item-size ml-auto">12kb</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- End File Manager - Cards -->
          </div>