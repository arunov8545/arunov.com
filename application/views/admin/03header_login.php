<!doctype html>
<html class="no-js h-100" lang="en"> 
<head> 
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Shards Dashboard Pro - Premium Bootstrap 4 Admin Dashboard Template Pack</title> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.3.1" href="<?php echo base_url(); ?>assets/admin/styles/shards-dashboards.1.3.1.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/styles/extras.1.3.1.min.css">
    <script async defer src="https://buttons.github.io/buttons.js"></script>
  </head>
  <body class="h-100">  
    <div class="container-fluid icon-sidebar-nav h-100">
      <div class="row h-100">
        <!-- Main Sidebar -->
        <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
          <div class="main-navbar">
            <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
              <a class="navbar-brand w-100 mr-0" href="#" style="line-height: 25px;">
                <div class="d-table m-auto">
                  <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 25px;" src="<?php echo base_url(); ?>assets/admin/images/shards-dashboards-logo.svg" alt="Shards Dashboard">
                </div>
              </a>
              <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                <i class="material-icons">&#xE5C4;</i>
              </a>
            </nav>
          </div>
          <form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
            <div class="input-group input-group-seamless ml-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="fas fa-search"></i>
                </div>
              </div>
              <input class="navbar-search form-control" type="text" placeholder="Search for something..." aria-label="Search">
            </div>
          </form>
          <div class="nav-wrapper">
            <h6 class="main-sidebar__nav-title">Dashboards</h6>
            <ul class="nav nav--no-borders flex-column">
              <li class="nav-item">
                <a class="nav-link " href="<?php echo base_url(); ?>admin">
                  <i class="material-icons">&#xE917;</i>
                  <span>Analytics</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/ecommerce">
                  <i class="material-icons">&#xE8D1;</i>
                  <span>Online Store</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/blog_overview">
                  <i class="material-icons">edit</i>
                  <span>Personal Blog</span>
                </a>
              </li>
            </ul>
            <h6 class="main-sidebar__nav-title">Templates</h6>
            <ul class="nav nav--no-borders flex-column">
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">
                  <i class="material-icons">&#xE7FD;</i>
                  <span>User Account</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small">
                  <a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/user_profile">User Profile</a>
                  <a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/user_profile_lite">User Profile Lite</a>
                  <a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/edit_user_profile">Edit User Profile</a>
                  <a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/login">Login</a>
                  <a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/register">Register</a>
                  <a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/forgot_password">Forgot Password</a>
                  <a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/change_password">Change Password</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                  <i class="material-icons">&#xE2C7;</i>
                  <span>File Managers</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small">
                  <a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/file_manager_list">Files - List View</a>
                  <a class="dropdown-item " href="<?php echo base_url(); ?>admin/Dashboard/file_manager_cards">Files - Cards View</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/transaction_history">
                  <i class="material-icons">&#xE889;</i>
                  <span>Transaction History</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/calendar">
                  <i class="material-icons">calendar_today</i>
                  <span>Calendar</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/add_new_post">
                  <i class="material-icons">note_add</i>
                  <span>Add New Post</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/errors">
                  <i class="material-icons">error</i>
                  <span>Errors</span>
                </a>
                </li>
            </ul>
            <h6 class="main-sidebar__nav-title">Components</h6>
            <ul class="nav nav--no-borders flex-column">
              <li class="nav-item">
                <a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/componentsoverview">
                  <i class="material-icons">view_module</i>
                  <span>Overview</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/tables">
                  <i class="material-icons">table_chart</i>
                  <span>Tables</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/components_blog_posts">
                  <i class="material-icons">vertical_split</i>
                  <span>Blog Posts</span>
                </a>
              </li>
            </ul>
            <h6 class="main-sidebar__nav-title">Layouts</h6>
            <ul class="nav nav--no-borders flex-column">
              <li class="nav-item">
                <a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/header_navigation">
                  <i class="material-icons">view_day</i>
                  <span>Header Nav</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?php echo base_url(); ?>admin/Dashboard/icon_sidebar_nav">
                  <i class="material-icons">&#xE251;</i>
                  <span>Icon Sidebar</span>
                </a>
              </li>
            </ul>
          </div>
        </aside>
        <!-- End Main Sidebar --> 