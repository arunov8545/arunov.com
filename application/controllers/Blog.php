<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
  
	public function index()
	{
		$this->load->view('header');
		$this->load->view('blog/blog_home');
		$this->load->view('footer');
	}
 
	public function blog_single()
	{
		$this->load->view('header');
		$this->load->view('blog/blog_single');
		$this->load->view('footer');
	} 
}
