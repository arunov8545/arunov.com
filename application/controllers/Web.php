<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {
 
	public function index()
	{
		$this->load->view('website/header');
		$this->load->view('website/home');
		$this->load->view('website/footer');
	}
 
	public function about()
	{
		$this->load->view('website/header');
		$this->load->view('website/about');
		$this->load->view('website/footer');
	}

	public function services()
	{
		$this->load->view('website/header');
		$this->load->view('website/services');
		$this->load->view('website/footer');
	}
 
	public function portfolio()
	{
		$this->load->view('website/header');
		$this->load->view('website/portfolio');
		$this->load->view('website/footer');
	}
		 
	public function blog()
	{
		$this->load->view('website/header');
		$this->load->view('website/blog');
		$this->load->view('website/footer');
	}
 
	public function blog_page()
	{
		$this->load->view('website/header');
		$this->load->view('website/blog_page');
		$this->load->view('website/footer');
	}
 
	public function contact()
	{
		$this->load->view('website/header');
		$this->load->view('website/contact');
		$this->load->view('website/footer');
	}
 
}
