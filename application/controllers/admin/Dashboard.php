<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/06website_overview');
		$this->load->view('admin/02footer');
	}
    
	public function BlogOverview()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/07blog_overview');
		$this->load->view('admin/02footer');
	}
        
	public function AdminPanel()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/08admin_overview');
		$this->load->view('admin/02footer');
	}
        
	public function Portfolio()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/09portfolio');
		$this->load->view('admin/02footer');
	}
	
	public function Testimonials()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/10testimonials');
		$this->load->view('admin/02footer');
	}
    
	public function NewBlogPost()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/11NewBlogPost');
		$this->load->view('admin/02footer');
	}
    
	public function ViewBlogPosts()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/12ViewBlogPosts');
		$this->load->view('admin/02footer');
	}
    
	public function ContactMe()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/13contactme');
		$this->load->view('admin/02footer');
	}
    
	public function calendar()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/calendar');
		$this->load->view('admin/02footer');
	}
    
	public function change_password()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/change_password');
		$this->load->view('admin/02footer');
	}

	public function components_overview()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/components_overview');
		$this->load->view('admin/02footer');
	}
	
	public function edit_user_profile()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/edit_user_profile');
		$this->load->view('admin/02footer');
	}
    
	public function errors()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/errors');
		$this->load->view('admin/02footer');
	}
    
	public function file_manager_cards()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/file_manager_cards');
		$this->load->view('admin/02footer');
	}
    
	public function file_manager_list()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/file_manager_list');
		$this->load->view('admin/02footer');
	}
    
	public function forgot_password()
	{
		$this->load->view('admin/03header_login');
		$this->load->view('admin/forgot_password');
		$this->load->view('admin/02footer');
	}
    
	public function icon_sidebar_nav()
	{  
		$this->load->view('admin/icon_sidebar_nav');  
	}
    
	public function login()
	{
		$this->load->view('admin/03header_login');
		$this->load->view('admin/login');
		$this->load->view('admin/02footer');
	}
    
	public function register()
	{
		$this->load->view('admin/03header_login');
		$this->load->view('admin/register');
		$this->load->view('admin/02footer');
	}

	public function transaction_history()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/transaction_history');
		$this->load->view('admin/02footer');
	}
    
	public function user_profile()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/user_profile');
		$this->load->view('admin/02footer');
	}
    
	public function user_profile_lite()
	{
		$this->load->view('admin/01header');
		$this->load->view('admin/user_profile_lite');
		$this->load->view('admin/02footer');
	}
}
